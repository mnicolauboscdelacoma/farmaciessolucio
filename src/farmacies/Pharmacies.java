package farmacies;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Pharmacies.java        3.0 13/03/2022
 *
 * Models the program.
 *
 * Copyright 2022 Marc Nicolau <mnicol29@xtec.cat>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Pharmacies {

    /**
     * Camí del fitxer que conté les farmàcies
     */
    private final static String PHARMACIES_PATH = "farmacies.csv";

    /**
     * Quantitat màxima de farmàcies en un municipi *
     */
    public final static int MAX_PHARMACIES_TOWN = 100;

    /**
     * Indica el nom d'un municipi.
     *
     * @param provinceId codi de província
     * @param townId	codi de municipi
     * @return El nom del municipi o bé null si no el troba
     */
    public String townName(int provinceId, int townId) {
        return searchTownName(provinceId, townId);
    }

    /**
     * Obté la quantitat total de farmàcies en una província.
     *
     * @param provinceId el codi de la província.
     * @return la quantitat de farmàcies de la província.
     */
    public int pharmaciesInProvince(int provinceId) {

        int count = 0;

        try ( Scanner file = new Scanner(Paths.get(PHARMACIES_PATH))) {
            String[] pharmacyLine;
            // saltar la capçalera del fitxer (primera linia)
            file.nextLine();
            while (file.hasNextLine()) {
                pharmacyLine = file.nextLine().split("[;]");
                if (Integer.parseInt(pharmacyLine[2]) == provinceId) {
                    count++;
                }
            }
        } catch (IOException ex) {
            System.out.println(ex);
            count = 0;
        }
        return count;
    }

    /**
     * Obté les farmàcies d'un municipi en forma d'array on a cada posició hi ha
     * el codi de la farmàcia i el nom de la farmàcia (o del seu propietari).
     *
     * @param provinceId codi de província
     * @param townId	codi de municipi
     * @param pharmacies array amb totes les farmàcies del municipi
     * @return la quantitat de farmàcies obtingudes en el municipi indicat
     */
    public int loadTownPharmacies(int provinceId, int townId, Pharmacy[] pharmacies) {
        int nDades = 0;

        try ( Scanner file = new Scanner(Paths.get(PHARMACIES_PATH))) {
            // passar per alt la primera línia del fitxer
            file.nextLine();
            // tractar la resta de línies del fitxer
            String[] lineData;
            while (file.hasNextLine() && nDades < MAX_PHARMACIES_TOWN) {
                lineData = file.nextLine().split("[;]");
                // inicialitzar la tupla de la posició 'nDades'
                pharmacies[nDades] = new Pharmacy();
                pharmacies[nDades].pharmacyId = lineData[0];
                pharmacies[nDades].pharmacyName = lineData[1];
                // tenim una farmàcia més a l'array.
                nDades++;
            }
        } catch (IOException ex) {
            System.out.println("Error " + ex);
            nDades = 0;
            pharmacies = null;
        }
        // retorna la quantitat de dades que realment conté l'array "pharmacies"
        // després d'executar el bucle
        return nDades;
    }

    /**
     * Genera un fitxer orientat a byte amb les farmàcies d'un municipi. En el
     * fitxer només s'hi desa el codi de cada farmàcia. El nom del fitxer que
     * s'ha de generar és el nom del municipi i extensió .dat.
     *
     * @param provinceId codi de província
     * @param townId codi de municipi
     */
    public void townPharmacies(int provinceId, int townId) {

        // obtenir el nom del municipi
        String townName = searchTownName(provinceId, townId);
        // si s'ha trobat el municipi -> generar fitxer orientat a byte.
        if (townName != null) {
            try ( RandomAccessFile file = new RandomAccessFile(townName + ".dat", "rw")) {
                Pharmacy[] farmacies = new Pharmacy[MAX_PHARMACIES_TOWN];
                int nDades = loadTownPharmacies(provinceId, townId, farmacies);
                for (int i = 0; i < nDades; i++) {
                    file.writeUTF(farmacies[i].pharmacyId);                    
                }
            } catch (IOException e) {
                System.out.println("Error " + e);
            }
        }
    }

    /**
     * Obtenir el nom del municipi en funció del seu codi de província, de
     * comarca i de municipi.
     *
     * @param provinceId codi de província
     * @param townId codi de municipi
     * @return el nom del municipi o bé null si no el troba.
     */
    public String searchTownName(int provinceId, int townId) {
        boolean trobat = false;
        String[] townData = null;

        try ( Scanner lector = new Scanner(new File(PHARMACIES_PATH))) {
            // llegir la capçalera
            lector.nextLine();
            while (lector.hasNext() && !trobat) {
                townData = lector.nextLine().split(";");
                if (Integer.parseInt(townData[2]) == provinceId
                        && Integer.parseInt(townData[3]) == townId) {
                    trobat = true;
                }
            }
            if (trobat) {
                return townData[4];
            } else {
                return null;
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error " + e);
            return null;
        }
    }

    /**
     * Mostra les dades de les farmàcies d'un array de farmàcies.
     *
     * @param pharmacies l'array que conté les dades.
     * @param size la quantitat de dades vàlides que conté l'array
     */
    public void showPharmaciesFromArray(Pharmacy[] pharmacies, int size) {
        for (int i = 0; i < size; i++) {
            pharmacies[i].show();
        }
    }

    /**
     * Obté les dades d'una farmàcia. Aquestes dades es cerquen en un array de
     * farmàcies.
     *
     * @param pharmacies l'array que conté totes les farmàcies.
     * @param size la quantitat de farmàcies vàlides que conté l'array.
     * @param pharmacyId l'identificador de la farmàcia que es vol cercar.
     * @return la tupla Pharmacy que conté les dades que es volien trobar o bé
     * null si no es troba.
     */
    private Pharmacy searchPharmacy(Pharmacy[] pharmacies, int size, String pharmacyId) {
        int i = 0;
        boolean trobat = false;

        while (i < size && !trobat) {
            if (pharmacies[i].pharmacyId.equals(pharmacyId)) {
                trobat = true;
            } else {
                i++;
            }
        }
        if (trobat) {
            return pharmacies[i];
        } else {
            return null;
        }
    }
}
