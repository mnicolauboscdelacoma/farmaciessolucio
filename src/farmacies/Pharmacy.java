package farmacies;

/*
 * Pharmacy.java        3.0 13/03/2022
 *
 *
 * Copyright 2022 Marc Nicolau Reixach <marc.nicolau@infobosccoma.net>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Pharmacy {

    String pharmacyId, pharmacyName;

    public void show() {
        System.out.println("CODI: " + pharmacyId);
        System.out.println("NOM: " + pharmacyName);
        System.out.println("---------------------------");
    }
}
