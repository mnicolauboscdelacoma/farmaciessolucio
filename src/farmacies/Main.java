package farmacies;

/*
 * Main.java        3.0 13/03/2022
 *
 * Models the program.
 *
 * Copyright 2022 Marc Nicolau Reixach <mnicol29@xtec.cat>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Pharmacies pharm = new Pharmacies();
        Pharmacy[] pharmacies = new Pharmacy[Pharmacies.MAX_PHARMACIES_TOWN];

        System.out.println("Mostrant el nom del municipi 114 de la província 17...");
        System.out.println(pharm.townName(17, 114));
        System.out.println();

        System.out.println("Mostrant la quantitat de farmàcies de la província 17...");
        System.out.println(pharm.pharmaciesInProvince(17));
        System.out.println();

        System.out.println("S'estan carregant i mostrant les farmàcies del municipi 114 de la província 17...");
        int nDades = pharm.loadTownPharmacies(17, 114, pharmacies);
        System.out.println(nDades + " farmàcies");
        pharm.showPharmaciesFromArray(pharmacies, nDades);
        System.out.println();

        System.out.println("S'està generant el fitxer amb les farmàcies del del municipi 114 de la província 17...");
        pharm.townPharmacies(17, 114);
        System.out.println("S'ha creat el fitxer");
        System.out.println();

        System.out.println("S'ha creat el fitxer");
        System.out.println();
    }
}
